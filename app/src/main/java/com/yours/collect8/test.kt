/*
 * Complete the 'repeatedString' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. LONG_INTEGER n
 */

fun CamelCase(str: String): String {

    // code goes here
    var camelCaseStr :String = str[0].toLowerCase().toString()
    str.forEachIndexed { index, char ->
        if (index!=0){
            if (isCharIsInTheAlphabet(str[index-1])&&isCharIsInTheAlphabet(char)){
                camelCaseStr += char.toLowerCase()
            }else if (!isCharIsInTheAlphabet(str[index -1])&&isCharIsInTheAlphabet(char)){
                camelCaseStr += char.toUpperCase()
            }
        }

    }
    return camelCaseStr;
}

fun isCharIsInTheAlphabet(c: Char): Boolean {
    return (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
}

fun main() {
    println(CamelCase(readLine().toString()))
}