fun StarRating(str: String): String {
    var numbOfFullStars = Character.getNumericValue(str[0])
    var isThereAhalffStar = false
    var afterDotNumber = str.substring(2).toInt()
    if (afterDotNumber< 10){
        afterDotNumber *= 10
    }
    if (afterDotNumber >= 75) {
        numbOfFullStars++
    } else if (afterDotNumber > 25) {
        isThereAhalffStar = true
    }
    var numberOFEmptyStarts : Int
    if (isThereAhalffStar) {
        numberOFEmptyStarts = 4 - numbOfFullStars
    } else {
        numberOFEmptyStarts = 5 - numbOfFullStars
    }
    var result = ""
    (1..numbOfFullStars).forEach {
        result += "full "
    }
    if (isThereAhalffStar) {
        result += "half "
    }
    (1..numberOFEmptyStarts).forEach {
        result += "empty "
    }
    // code goes here
    return result;
}

fun main() {
    println(StarRating(readLine().toString()))
}