package com.yours.collect8

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.huhx0015.hxaudio.audio.HXMusic
import com.yours.collect8.data.ArenaTheme
import com.yours.collect8.data.ArizonaTheme
import com.yours.collect8.data.EgyptTheme
import com.yours.collect8.data.NewYourTheme
import com.yours.collect8.ui.arena.ArenaFragment
import com.yours.collect8.ui.arena.model.ArenaArgsConfig
import com.yours.collect8.ui.skillz.SkillzFragmentDirections
import com.yours.collect8.utils.CallListener

class MainActivity : AppCompatActivity() {


    var receiver: CallListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment? ?: return

        // Set up Action Bar
        val navController = host.navController
        var isFromSkillz: Boolean = false
        var arenaArgsConfig: ArenaArgsConfig? = null
        intent.extras?.let {
            isFromSkillz = it.getBoolean("skillz")
            arenaArgsConfig = it.getParcelable("skillz_param")
        }

        if (isFromSkillz && arenaArgsConfig != null) {
            val action = SkillzFragmentDirections.arenaNavigationAction(
                arenaArgsConfig!!.speed,
                arenaArgsConfig!!.level,
                arenaArgsConfig!!.time,
                arenaArgsConfig!!.maxPoints,
                arenaArgsConfig!!.difficulty,
                theme = listOf<ArenaTheme>(ArizonaTheme(), EgyptTheme(), NewYourTheme()).random()
            )
            navController.navigate(action)
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }


            Log.d("NavigationActivity", "Navigated to $dest")
        }

        val testclass = ChangeFingerPrint()
        testclass.doSomthing()

        val filter = IntentFilter()
        filter.addAction("android.intent.action.PHONE_STATE");
        receiver = CallListener {
            val fragment = (host.getChildFragmentManager().getFragments()
                .get(0) as ArenaFragment)
            if (!fragment.isGamePaused()) {
                fragment.pauseGame()
                fragment.showPauseDialog()
            }
        }
        registerReceiver(receiver, filter)
    }

    override fun onPause() {
        super.onPause()
        pauseMusic()
    }

    fun pauseMusic() {
        HXMusic.pause()
    }

    override fun onResume() {
        super.onResume()
        resumeMusic()
    }

    fun resumeMusic() {
        HXMusic.resume(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
        HXMusic.clear()
    }
}