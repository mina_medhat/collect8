package com.yours.collect8.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class CallListener(val onCallReceived: () -> Unit) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        onCallReceived.invoke()
    }
}