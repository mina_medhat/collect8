package com.yours.collect8.utils.CustomViews

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.yours.collect8.R
import com.yours.collect8.data.CustomDotConfig
import com.yours.collect8.data.RandomBall
import com.yours.collect8.ui.arena.model.PowerUp


class DotCustomView(
    context: Context,
    attrs: AttributeSet? = null,
    customDotConfig: CustomDotConfig
) :
    FrameLayout(context, attrs) {
    override fun getTag(): String {
        return super.getTag() as String
    }

    lateinit var imgBaseDot: ImageView
    lateinit var frameMovable: FrameLayout
    lateinit var imgMovableDot: ImageView
    lateinit var tvNumber: TextView
    var keyColor: String = ""

    var dotSolidColor = ContextCompat.getColor(context, R.color.baseDoteSolid)
    private var borderColor = ContextCompat.getColor(context, R.color.baseDoteBorder)
    private var borderWidth = 5.0f

    var movableDotType = 1
    var movableDotResId: Int? = null

    // View size in pixels
    private var size: Int = 0

    //animation
    var shake: ObjectAnimator? = null

    init {
        //setupAttributes(attrs)
        foregroundGravity = Gravity.CENTER
        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.item_dot_frame, this, true)
        imgBaseDot = getChildAt(0) as ImageView
        frameMovable = getChildAt(1) as FrameLayout
        imgMovableDot = frameMovable.getChildAt(0) as ImageView
        tvNumber = frameMovable.getChildAt(1) as TextView
        showMovableDot(customDotConfig.movableDotVisability)
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        size = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(size, size)
    }

    private fun setupAttributes(attrs: AttributeSet?) {

        val typedArray = context.theme.obtainStyledAttributes(
            attrs, R.styleable.DotFrameCustomView,
            0, 0
        )

        movableDotType = typedArray.getInt(R.styleable.DotFrameCustomView_movableDotType, 1).toInt()

        typedArray.recycle()
    }

    fun startDotFlowing() {
        showMovableDot(true)
        animateMovableDot()
    }

    fun startDotFlowingWithSpecicalColorAndPowerUp(randomBall: RandomBall, powerUp: PowerUp?) {
        imgMovableDot.setImageDrawable(ContextCompat.getDrawable(context, randomBall.resId))
        movableDotResId = randomBall.resId
        tvNumber.text = randomBall.num.toString()
        showMovableDot(true)
        animateMovableDot()
    }

    fun shakeOn8ReachAnimation() {
        shake = ObjectAnimator
           // .ofFloat(frameMovable, "translationX", 0f, 10f, 25f, 10f, 0f, -10f, -25f, -10f, 0f)
            .ofFloat(frameMovable, "translationX",  20f ,0f, -20f, 0f)
            .setDuration(400);
        shake?.repeatCount = ObjectAnimator.INFINITE;
        shake?.repeatMode = ObjectAnimator.RESTART
        shake?.start();
    }

    fun cancelShakeAnimation() {
        shake?.let {
            if (it.isRunning) {
                it.cancel()
            }
        }
    }

    fun animateMovableDot() {
        val set = AnimatorSet()
        val scaleXAnim = ObjectAnimator.ofFloat(frameMovable, "scaleX", 0.0f, 1.0f)
        scaleXAnim.duration = 200
        val scaleYAnim = ObjectAnimator.ofFloat(frameMovable, "scaleY", 0.0f, 1.0f)
        scaleYAnim.duration = 200
        set.playTogether(scaleXAnim, scaleYAnim)
        set.start()
    }

    fun moveMovableDotBackToOriginalPoisition(
        parent: LinearLayout,
        startX: Float,
        deltaX: Float,
        startY: Float,
        deltaY: Float
    ) {
        parent.overlay.add(imgMovableDot)
        val set = AnimatorSet()
        val scaleXAnim = ObjectAnimator.ofFloat(imgMovableDot, "translationX", startX, imgBaseDot.x)
        scaleXAnim.duration = 2000
        val scaleYAnim = ObjectAnimator.ofFloat(imgMovableDot, "translationY", startY, imgBaseDot.y)
        scaleYAnim.duration = 2000
        scaleYAnim.doOnEnd {
            parent.overlay.remove(imgMovableDot)
        }
        set.playTogether(scaleXAnim, scaleYAnim)
        set.start()
    }


    fun showMovableDot(isVisible: Boolean) {
        frameMovable.isVisible = isVisible
    }

    fun isMovableDotVisible(): Boolean {
        return frameMovable.isVisible
    }

    fun resetMovableFrame() {
        this.removeView(frameMovable)
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_dot_frame, this, true)
        if (view.getParent() != null) {
            (view.getParent() as ViewGroup).removeView(view) // <- fix
        }
        this.addView(view)
        frameMovable = view as FrameLayout
        imgMovableDot = frameMovable.getChildAt(0) as ImageView
        tvNumber = frameMovable.getChildAt(1) as TextView
        tvNumber.text = 0.toString()
    }

    fun startCollectScoreAniamtion(lnParent: LinearLayout) {
        lnParent.overlay.add(frameMovable)
        val metrics = lnParent.resources.displayMetrics
        val oldx = frameMovable.x
        val oldy = frameMovable.y
        val transactionX =
            if (frameMovable.x > (lnParent.width / 2)) {
                -((lnParent.width / 2) - (lnParent.width - (frameMovable.x + (frameMovable.width / 2))))
            } else {
                ((lnParent.width / 2) - (frameMovable.x + (frameMovable.width / 2)))
            }
        val transactionY = frameMovable.y - 20

//        val transactionXAnim = ObjectAnimator.ofFloat(imgMovableDot, "translationX", transactionX)
//        transactionXAnim.duration = 1000
//        transactionXAnim.interpolator = AccelerateInterpolator()
//        val transactionYAnim = ObjectAnimator.ofFloat(imgMovableDot, "translationX", -transactionY)
//        transactionYAnim.duration = 1000
//        transactionYAnim.interpolator = AccelerateInterpolator()
//        val set = AnimatorSet()
//        set.playTogether(transactionXAnim, transactionYAnim)
//        set.start()
        /* var runnable: Runnable? = null
         runnable = Runnable {
             val animation = frameMovable.animate()
                 .translationX(transactionX)
                 .translationY(-transactionY)
                 .alpha(0.5f)
                 .setInterpolator(AccelerateInterpolator())
                 .setDuration(500)
         }
         runnable.run()*/
        val animationX = ObjectAnimator.ofFloat(frameMovable, "translationX", transactionX).apply {
            duration = 500
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {

                }
            })
        }

        val animationY = ObjectAnimator.ofFloat(frameMovable, "translationY", -transactionY).apply {
            duration = 500
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {

                }
            })
        }
        val animationAlpha = ObjectAnimator.ofFloat(frameMovable, "alpha", 1f, 0f).apply {
            duration = 500
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    frameMovable.alpha = 1f
                    showMovableDot(false)
                }
            })
        }
        val animationX2 =
            ObjectAnimator.ofFloat(frameMovable, "translationX", oldx - frameMovable.x).apply {
                duration = 100
                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {

                    }
                })
            }

        val animationY2 =
            ObjectAnimator.ofFloat(frameMovable, "translationY", oldy - frameMovable.y).apply {
                duration = 100
                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        //lnParent.overlay.remove(frameMovable)
                        frameMovable.clearAnimation()
                    }
                })
            }

        val set1 = AnimatorSet().apply {
            playTogether(animationX, animationY, animationAlpha)
            start()
        }
        val set2 = AnimatorSet().apply {
            playTogether(animationX2, animationY2)
            startDelay = 600L
            start()
        }

//        handler.postDelayed({
//            frameMovable.clearAnimation()
//            frameMovable.animate()
//                .translationX(-transactionX)
//                .translationY(transactionY)
//                .alpha(1f)
////                .setInterpolator(AccelerateInterpolator())
//                .setDuration(500).withEndAction {
//                    frameMovable.clearAnimation()
//                }
//        }, 600);

//        animation.setListener(object : Animator.AnimatorListener {
//            override fun onAnimationStart(p0: Animator?) {
//
//            }
//
//            override fun onAnimationEnd(p0: Animator?) {
//                p0?.removeAllListeners()
//                animation.ca
//                animation.setListener(null)
//                resetMovableFrame()
//            }
//
//            override fun onAnimationCancel(p0: Animator?) {
//                TODO("Not yet implemented")
//            }
//
//            override fun onAnimationRepeat(p0: Animator?) {
//                TODO("Not yet implemented")
//            }
//        })

//
//        val transactionX = ObjectAnimator.ofFloat(
//            imgMovableDot,
//            "translationX",
//            startX,
//            imgBaseDot.x
//        )
//        transactionX.duration = 2000
    }

}