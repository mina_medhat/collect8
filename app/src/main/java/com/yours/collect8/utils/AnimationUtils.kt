package com.yours.collect8.utils

import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import com.plattysoft.leonids.ParticleSystem
import com.yours.collect8.R


object AnimationUtils {
    fun fillProgressWithAnimation(v: View, toProgress: Int) {
        val animation = ObjectAnimator.ofInt(v, "progress", toProgress)
        animation.duration = 200
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }

}

