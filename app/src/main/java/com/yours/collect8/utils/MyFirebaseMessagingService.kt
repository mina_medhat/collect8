package com.yours.collect8.utils

import android.util.Log
import com.application.isradeleon.notify.Notify
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.data.isNotEmpty()) {
            Log.d("FCM", "Message data payload: ${remoteMessage.data}")
        }
        val title = remoteMessage.notification?.title ?: "collect8"
        val body = remoteMessage.notification?.body ?: "collect 8 body"
        Notify.build(applicationContext)
            .setTitle(title)
            .setContent(body)
            .show(); // Show notification
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }
}