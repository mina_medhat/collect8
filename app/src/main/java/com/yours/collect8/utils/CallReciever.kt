package com.yours.collect8.utils

import android.content.Context
import java.util.*

class CallReciever : PhonecallReceiver() {
    override fun onIncomingCallStarted(ctx: Context?, number: String?, start: Date?) {
    }

    override fun onOutgoingCallStarted(ctx: Context?, number: String?, start: Date?) {}

    override fun onIncomingCallEnded(ctx: Context?, number: String?, start: Date?, end: Date?) {}

    override fun onOutgoingCallEnded(ctx: Context?, number: String?, start: Date?, end: Date?) {}

    override fun onMissedCall(ctx: Context?, number: String?, start: Date?) {}
}