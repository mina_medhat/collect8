package com.yours.collect8.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson


class Prefs(context: Context) {
    val PREFS_FILENAME = "com.yours.collect8"
    val tutorialCompletedKey = "TUTORIAL_COMPLETED_KEY"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);
    val gson = Gson()

    //my prefs
    var tutorialCompleted: Boolean
        get() = prefs.getBoolean(tutorialCompletedKey, false)
        set(value) = prefs.edit().putBoolean(tutorialCompletedKey, value).apply()

}