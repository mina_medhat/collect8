package com.yours.collect8.utils.CustomViews

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.View.DragShadowBuilder
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.yours.collect8.R


class BaseDotCustom(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
    override fun getTag(): String {
        return super.getTag() as String
    }
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    var dotSolidColor = ContextCompat.getColor(context, R.color.baseDoteSolid)
    private var borderColor = ContextCompat.getColor(context, R.color.baseDoteBorder)
    private var borderWidth = 5.0f

    // View size in pixels
    private var size: Int = 0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        size = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        // call the super method to keep any drawing from the parent side.
        super.onDraw(canvas)
        drawBaseDot(canvas)
    }


    private fun drawBaseDot(canvas: Canvas) {
        paint.color = dotSolidColor
        paint.style = Paint.Style.FILL

        val radius = size / 2f

        canvas.drawCircle(size / 2f, size / 2f, radius, paint)

        paint.color = borderColor
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = borderWidth

        canvas.drawCircle(size / 2f, size / 2f, radius - borderWidth / 2f, paint)
    }
}


class MyDragShadowBuilder(view: View) : DragShadowBuilder(view) {
    override fun onProvideShadowMetrics(outShadowSize: Point, outShadowTouchPoint: Point) {
        outShadowSize.set(1, 1)
        outShadowTouchPoint.set(0, 0)
    }
}


class MyShadowBuilder(val context: Context,val v: View) : View.DragShadowBuilder(v) {

//    private val shadowBorder = ColorDrawable(Color.BLACK)
//
//    private val shadow = ColorDrawable(Color.parseColor(v.tag.toString()))

    // Defines a callback that sends the drag shadow dimensions and touch point back to the system.
    override fun onProvideShadowMetrics(size: Point, touch: Point) {
        // First, we define the shadow width and height. In our example, it will be
        // half of the size of the view that's been dragged.
        //val width: Int = view.width / 2
        //val height: Int = view.height / 2

        // The drag shadow is a `ColorDrawable`. This sets its dimensions to be the same as the
        // `Canvas` that the system will provide. We leave some room (four pixels) for the shadow border.
        //shadow.setBounds(4, 4, width - 4, height - 4)
        //shadowBorder.setBounds(0, 0, width, height)

        // Sets the size parameter's width and height values.
        // These get back to the system through the size parameter.
        //size.set(width, height)

        // Sets the touch point's position to be in the middle of the drag shadow.
        //touch.set(width / 2, height / 2)
        super.onProvideShadowMetrics(
            size,
            touch
        );
    }

    // Defines a callback that draws the drag shadow in a `Canvas` that the
    // system constructs from the dimensions passed in `onProvideShadowMetrics()`.
    override fun onDrawShadow(canvas: Canvas) {

        // Draws the border drawable first.
//        val greyFilter = PorterDuffColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY)
//        ((v as FrameLayout).getChildAt(0) as ImageView).setImageDrawable(ContextCompat.getDrawable(context,R.drawable.moving_dot_2))
        v.draw(canvas)
        v.alpha = 1f


        // Draws the actual shadow drawable onto the `Canvas` passed in
        // from the system so that the shadow content is above its border.
        //shadow.draw(canvas)
    }
}