package com.yours.collect8.utils

import android.text.format.DateUtils

fun formatDuration(seconds: Long): String = if (seconds < 10) {
    "00:0$seconds"
} else if (seconds < 60) {
    "00:$seconds"
} else {
    DateUtils.formatElapsedTime(seconds)
}
