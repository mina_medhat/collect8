package com.yours.collect8.utils

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import com.yours.collect8.MainActivity


fun Context.startArenaScreen() =
    Intent(this, MainActivity::class.java).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }

fun getScreenWidth(): Int {
    return Resources.getSystem().getDisplayMetrics().widthPixels
}


