package com.yours.collect8.ui.arena

import androidx.lifecycle.MutableLiveData
import com.yours.collect8.ui.arena.model.PowerUp

class ArenaViewState(
    var timeValue: MutableLiveData<String> = MutableLiveData(),
    var updateStreakProgress: MutableLiveData<Int> = MutableLiveData(),
    var updateScore: MutableLiveData<Int> = MutableLiveData(),
    var back2xPowerAnimationRunning: MutableLiveData<Boolean> = MutableLiveData(),
    var updatePowerUp: MutableLiveData<List<PowerUp>> = MutableLiveData(),
    var backNavigationAction: MutableLiveData<Boolean> = MutableLiveData(),
    var restartGameMusic: MutableLiveData<Boolean> = MutableLiveData(),
    var resumeMusic: MutableLiveData<Boolean> = MutableLiveData(),
    var clear: MutableLiveData<Boolean> = MutableLiveData()
)