package com.yours.collect8.ui.splash

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.huhx0015.hxaudio.audio.HXMusic
import com.skillz.Skillz
import com.yours.collect8.Collect8App
import com.yours.collect8.R
import com.yours.collect8.databinding.ActivitySplashBinding
import com.yours.collect8.ui.arena.ArenaActivity
import com.yours.collect8.ui.tutorial.FragmentTutorial
import com.yours.collect8.utils.startArenaScreen


class SplashActivity : AppCompatActivity() {

    private var savedInstance: Bundle? = null
    private lateinit var binding: ActivitySplashBinding
    private lateinit var viewModel: SplashViewModel

    private var fragmetTutorial = FragmentTutorial()
    private val PERMISSION_READ_STATE: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = SplashViewModel()
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.imgSplashPlayNow.setOnClickListener {
            checkPermissions()
            viewModel.animateViewWithScaleOut(binding.lnSplashButtons).apply {
                duration = 1000
                start()
            }

            val bg: TransitionDrawable =
                (binding.rlSplashBackground.getBackground() as TransitionDrawable)
            bg.startTransition(1000)
            Handler(Looper.getMainLooper()).postDelayed({
                bg.resetTransition()
//                val i = Intent(this,ArenaActivity::class.java)
//                startActivity(i)
                Skillz.launch(this)
                finish()
//                startArenaScreen()
            }, 1000)
        }
        binding.imgQuestionTutorial.setOnClickListener {
            fragmetTutorial = FragmentTutorial()
            if (savedInstanceState == null) { // initial transaction should be wrapped like this
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_tutorial, fragmetTutorial).addToBackStack("tag")
                    .commitAllowingStateLoss()
            }
            //  startArenaScreen()
        }
        savedInstance = savedInstanceState
        checkPermissions()
        startMusic()

    }

    private fun checkForTutorial(savedInstanceState: Bundle?) {
        Collect8App.prefs?.let {
            if (!it.tutorialCompleted) {
                if (savedInstanceState == null) { // initial transaction should be wrapped like this
                    supportFragmentManager.beginTransaction()
                        .add(R.id.fragment_tutorial, fragmetTutorial).addToBackStack("tag")
                        .commitAllowingStateLoss()
                }
                Collect8App.prefs?.tutorialCompleted = true
            }
        }
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf<String>(Manifest.permission.READ_PHONE_STATE),
                PERMISSION_READ_STATE
            );
        }
    }

    private fun startMusic() {
        HXMusic.music().load(R.raw.game_play)
            .looped(true)                // Sets the song to be looped. [OPTIONAL]
            .play(applicationContext);
    }

    override fun onPause() {
        super.onPause()
        HXMusic.pause()
    }

    override fun onResume() {
        super.onResume()
        HXMusic.resume(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_READ_STATE -> {
                if (grantResults.size > 0
                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    // permission granted!
                    // you may now do the action that requires this permission
                } else {
                    // permission denied
                }
                checkForTutorial(savedInstance)
                return
            }
        }
    }
}