package com.yours.collect8.ui.arena.business

import android.os.Handler
import android.os.Looper
import com.yours.collect8.data.Dot
import com.yours.collect8.ui.arena.model.InitialPowerUp
import com.yours.collect8.ui.arena.model.PowerUp

interface updatePointsUseCase {

    fun registerChangesListeners(
        streakProgressListener: UpdateStreakProgressListener,
        powerUpListener: UpdatePowerUpListener, scoreListener: UpdateScoreListener
    )

    fun updateScoreAndPowerUpsOnMerging(movedDot: Dot, droppedOnDot: Dot)
    fun updateScoreOnCollect8(dot8: Dot)
    fun applyPowerUpScoring(powerUpPos: PowerUp)
    fun decreaseScoring(pints: Int)
    fun resestStreak()
    fun applyPowerUpCommon(powerUp: PowerUp)
}

class updatePointsUseCaseImp(val scoring: Scoring, val powerUpping: PowerUpping) :
    updatePointsUseCase {
    lateinit var streakProgressListener: UpdateStreakProgressListener
    lateinit var powerUpListener: UpdatePowerUpListener
    lateinit var scoreListener: UpdateScoreListener
    override fun updateScoreAndPowerUpsOnMerging(movedDot: Dot, droppedOnDot: Dot) {
        val points = scoring.calculateMoveScore(movedDot, droppedOnDot)
        scoring.addToScore(points)
        scoreListener.updateScore(scoring.getTotalScore())
        if (scoring.isShouldIncreaseThePowerUpProgress(points) && powerUpping.isNotMaxPowerUpReach()) {
            powerUpping.increaseStreak()
            streakProgressListener.updateProgress(powerUpping.getCurrentStreak())
            if (powerUpping.getCurrentStreak() == 5) {
                powerUpping.addRandomPowerUp()
                resestStreak()
                powerUpping.increasePerks()
                powerUpListener.newPowerUp(powerUpping.getCurrentPowerUps())
            }
        } else {
            //do nothing :D
           //resestStreak()
        }
    }

    override fun resestStreak(){
        powerUpping.resetStreak()
        streakProgressListener.updateProgress(powerUpping.getCurrentStreak())
    }

    override fun registerChangesListeners(
        streakProgressListener: UpdateStreakProgressListener,
        powerUpListener: UpdatePowerUpListener,
        scoreListener: UpdateScoreListener
    ) {
        this.streakProgressListener = streakProgressListener
        this.powerUpListener = powerUpListener
        this.scoreListener = scoreListener
    }

    override fun updateScoreOnCollect8(dot8: Dot) {
        scoring.calculateAndAdd8ReachedScore(dot8)
        scoreListener.updateScore(scoring.getTotalScore())
    }

    override fun applyPowerUpScoring(powerUp: PowerUp) {
        scoring.setCurrentUsagePowerUp(powerUp)
        Handler(Looper.getMainLooper()).postDelayed({
            val initPowerUp = InitialPowerUp()
            powerUpping.setCurrentPowerUpHeaderText(initPowerUp.name)
            scoring.setCurrentUsagePowerUp(initPowerUp)
        }, powerUp!!.duration.toLong() * 1000)

    }

    override fun decreaseScoring(pints: Int) {
        if (scoring.getTotalScore() > pints) {
            scoring.decreaseScore(pints)
            scoreListener.updateScore(scoring.getTotalScore())
        }
    }

    override fun applyPowerUpCommon(powerUp: PowerUp) {
        powerUpping.decreasePerks()
        powerUpping.removePowerUp(powerUp)
    }

}

interface UpdateStreakProgressListener {
    fun updateProgress(streak: Int)
}

interface UpdatePowerUpListener {
    fun newPowerUp(currentPowerUps: List<PowerUp>)
}

interface UpdateScoreListener {
    fun updateScore(score: Int)
}