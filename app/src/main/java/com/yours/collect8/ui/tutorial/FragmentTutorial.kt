package com.yours.collect8.ui.tutorial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.denzcoskun.imageslider.models.SlideModel
import com.yours.collect8.R
import com.yours.collect8.databinding.FragmentTutorialBinding


class FragmentTutorial : Fragment() {
    private lateinit var _binding: FragmentTutorialBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTutorialBinding.inflate(inflater, container, false)
        val view = _binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding.imgClose.setOnClickListener{
            parentFragmentManager.beginTransaction()
                .remove(this).commit();
        }
        setupImageSilder()
    }

    private fun setupImageSilder() {
        val imageList = ArrayList<SlideModel>()
        imageList.add(SlideModel(R.drawable.tutorial_1))
        imageList.add(SlideModel(R.drawable.tutorial_2))
        imageList.add(SlideModel(R.drawable.tutorial_3))
        imageList.add(SlideModel(R.drawable.tutorial_4))
        imageList.add(SlideModel(R.drawable.tutorial_5))
        imageList.add(SlideModel(R.drawable.tutorial_6))
        imageList.add(SlideModel(R.drawable.tutorial_7))
        imageList.add(SlideModel(R.drawable.tutorial_8))
        _binding.imageSlider.setImageList(imageList)
    }



}