package com.yours.collect8.ui.arena.business


import android.widget.TextView
import com.yours.collect8.ui.arena.ArenaRandom
import com.yours.collect8.ui.arena.model.PowerUp


interface PowerUpping {
    fun isMaxPowerUpReach(): Boolean
    fun isNotMaxPowerUpReach(): Boolean
    fun increasePerks()
    fun decreasePerks()
    fun increaseStreak()
    fun resetStreak()
    fun getRandomPowerUp(): PowerUp
    fun addRandomPowerUp()
    fun getCurrentStreak(): Int
    fun getCurrentPowerUps(): List<PowerUp>
    fun removePowerUpAt(pos: Int)
    fun removePowerUp(powerUp: PowerUp)
    fun setCurrentPowerUpHeaderView(tvCurrentPowerUp: TextView)
    fun setCurrentPowerUpHeaderText(powerUpName: String)
}

class PowerUppingImp(val arenaRandom: ArenaRandom) : PowerUpping {
    var perks = 0
    var streak = 0
    val currentPowerUp = mutableListOf<PowerUp>()
    var viewPowerUpHeaderName: TextView? = null
    override fun isMaxPowerUpReach(): Boolean {
        return !isNotMaxPowerUpReach()
    }

    override fun isNotMaxPowerUpReach(): Boolean {
        return perks < 3
    }

    override fun increasePerks() {
        perks++
    }

    override fun decreasePerks() {
        perks--
    }

    override fun increaseStreak() {
        streak++
    }

    override fun resetStreak() {
        streak = 0
    }

    override fun getRandomPowerUp(): PowerUp {
        return arenaRandom.getRandomPowerUp(currentPowerUp)
    }

    override fun addRandomPowerUp() {
        if (currentPowerUp.size < 3)
            currentPowerUp.add(getRandomPowerUp())
    }

    override fun getCurrentStreak(): Int {
        return streak
    }

    override fun getCurrentPowerUps(): List<PowerUp> {
        return currentPowerUp
    }

    override fun removePowerUp(powerUp: PowerUp) {
        currentPowerUp.remove(powerUp)
    }

    override fun removePowerUpAt(pos: Int) {
        currentPowerUp.removeAt(pos)
    }

    override fun setCurrentPowerUpHeaderView(tvCurrentPowerUp: TextView) {
        viewPowerUpHeaderName = tvCurrentPowerUp
    }

    override fun setCurrentPowerUpHeaderText(powerUpName: String) {
        viewPowerUpHeaderName?.text = powerUpName
    }
}