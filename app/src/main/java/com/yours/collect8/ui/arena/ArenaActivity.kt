package com.yours.collect8.ui.arena

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.huhx0015.hxaudio.audio.HXMusic
import com.skillz.Skillz
import com.skillz.SkillzActivity
import com.yours.collect8.MainActivity
import com.yours.collect8.R
import com.yours.collect8.data.ArizonaTheme
import com.yours.collect8.ui.arena.model.ArenaArgsConfig

class ArenaActivity : SkillzActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arena)
//        val fragment = ArenaFragment2()
//        val bundle = Bundle()
//        bundle.putInt("size", 3)
//        bundle.putFloat("speed", 2f)
//        bundle.putInt("time", 120)
//        bundle.putInt("maxPointPerMatch", 200)
//        bundle.putInt("difficulty", 1)
//        bundle.putParcelable("theme", ArizonaTheme())
//        fragment.arguments = bundle
//        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                .add(R.id.fragment_container, fragment).addToBackStack("tag")
//                .commitAllowingStateLoss()
//        }
        if (Skillz.isMatchInProgress()) {
            Log.d("skillz mina", "onViewCreated: match started")
            val params = Skillz.getMatchRules();
            params.mapNotNull {
                Log.d("skillz mina", it.toString())
            }

            val arenaConfig =
                ArenaArgsConfig(
                    params.get("size")?.toInt() ?: 3,
                    params.get("speed")?.toFloat() ?: 2f,
                    params.get("time")?.toInt() ?: 120,
                    params.get("skillz_difficulty")?.toInt() ?: 10,
                    params.get("maxPointsPerMatch")?.toInt() ?: 200
                )

            val i = Intent(this, MainActivity::class.java)
            i.putExtra("skillz", true)
            i.putExtra("skillz_param", arenaConfig);
            startActivity(i)
            finish()
        }

    }
    override fun onPause() {
        super.onPause()
        pauseMusic()
    }

    fun pauseMusic() {
        HXMusic.pause()
    }

    override fun onResume() {
        super.onResume()
        resumeMusic()
    }

    fun resumeMusic() {
        HXMusic.resume(this)
    }

//    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
//        if (Skillz.isMatchInProgress()) {
//            Log.d("skillz mina", "onViewCreated: match started")
//            val params = Skillz.getMatchRules();
//            params.mapNotNull {
//                Log.d("skillz mina", it.toString())
//                //Toast.makeText(activity, it.toString(), Toast.LENGTH_SHORT).show()
//            }
//        }
//        return super.onCreateView(name, context, attrs)
//    }

}