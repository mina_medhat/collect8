package com.yours.collect8.ui.arena.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class ArenaArgsConfig(val level: Int,val speed: Float, val time: Int,val difficulty: Int,val maxPoints: Int) : Parcelable
