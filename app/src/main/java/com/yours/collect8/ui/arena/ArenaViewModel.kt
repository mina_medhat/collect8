package com.yours.collect8.ui.arena

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModel
import com.huhx0015.hxaudio.audio.HXMusic
import com.skillz.Skillz
import com.yours.collect8.data.Dot
import com.yours.collect8.data.sound.SoundManager
import com.yours.collect8.ui.arena.business.*
import com.yours.collect8.ui.arena.model.ArenaArgsConfig
import com.yours.collect8.ui.arena.model.InitialPowerUp
import com.yours.collect8.ui.arena.model.PowerUp
import com.yours.collect8.utils.CustomViews.DotCustomView
import com.yours.collect8.utils.formatDuration
import java.math.BigDecimal
import java.util.*

class ArenaViewModel : ViewModel() {


    private val TAG = "ArenaFragment_TAG"
    lateinit var flowingBallsTimer: TimeIntervalManager
    lateinit var timeTimer: TimeIntervalManager
    lateinit var arenaRandom: ArenaRandom
    lateinit var arenaArgsConfig: ArenaArgsConfig
    lateinit var scoring: Scoring
    lateinit var powerUpping: PowerUpping
    lateinit var updatePointsUseCase: updatePointsUseCase
    lateinit var flowingBallsUseCase: FlowingBallsUseCase
    lateinit var soundManager: SoundManager

    var totalTime = 0
    var timeNow: Int = 0
    var fifthPeriodTime: Int = 0
    var gamePaused: Boolean = false
    val viewState = ArenaViewState()
    private var isAppFrezzed: Boolean = false
    private var isGameFinished: Boolean = false

    private var isThereScoringDecreasing: Boolean = false
    private var scoringDecreasingValue: Int = 0
    private var currentFlowingPowerUp: PowerUp? = null

    val boardMap = mutableMapOf<String, Dot>()
    private var towXPowerHandler: Handler? = null
    private var rushPowerHandler: Handler? = null
    private var freezPowerHandler: Handler? = null
    private var towXRunnable: Runnable? = null
    private var rushRunnable: Runnable? = null
    private var freezRunnable: Runnable? = null

    fun init(context: Context, arenaArgsConfig: ArenaArgsConfig) {
        this.arenaArgsConfig = arenaArgsConfig
        flowingBallsTimer = TimeIntervalManagerImp(speed = arenaArgsConfig.speed)
        timeTimer = TimeIntervalManagerImp(1f, arenaArgsConfig.time)
        arenaRandom = ArenaRandomImp(context, arenaArgsConfig.level)
        totalTime = arenaArgsConfig.time
        timeNow = arenaArgsConfig.time
        fifthPeriodTime = arenaArgsConfig.time / 5
        // init business classes
        scoring = ScoringImp(arenaArgsConfig.maxPoints)
        powerUpping = PowerUppingImp(arenaRandom)
        updatePointsUseCase = updatePointsUseCaseImp(scoring, powerUpping)
        flowingBallsUseCase = FlowingBallsUseCaseImp(arenaRandom)
        soundManager = SoundManager(context)
        // soundManager.playGamePlay()

        //register changes listeners
        updatePointsUseCase.registerChangesListeners(object : UpdateStreakProgressListener {
            override fun updateProgress(streak: Int) {
                viewState.updateStreakProgress.value = streak * 100 / 5
            }

        }, object : UpdatePowerUpListener {
            override fun newPowerUp(currentPowerUp: List<PowerUp>) {
                viewState.updatePowerUp.value = currentPowerUp
                soundManager.playProgressCompeletedSoundEffect()
            }
        }, object : UpdateScoreListener {
            override fun updateScore(score: Int) {
                viewState.updateScore.value = score
            }
        })
        soundManager.playStartGameSoundEffect()
        viewState.restartGameMusic.value = true
    }


    fun startTimeCountDown() {
        timeTimer.resumeTimerWithComplete({
            resumeTimerLogic(it)
        }, {
            Log.d(TAG, "startTimeCountDown: completed")
            timeTimer.stopTimer()
            isGameFinished = true
            viewState.backNavigationAction.value = true
            soundManager.playGameFinishSoundEffect()
            soundManager.clearCurrentSoundEffect()
        })
    }

    private fun resumeTimerLogic(it: Long) {
        Log.d(TAG, "startTimeCountDown: ${it.toInt()}")
        timeNow = totalTime - it.toInt()
        Log.d(TAG, "startTimeCountDown time now: $timeNow")
        if (isThereScoringDecreasing) {
            updatePointsUseCase.decreaseScoring(scoringDecreasingValue)
            soundManager.playBoardCompletedSoundEffect()
        }
        if (timeNow == fifthPeriodTime) {
            flowingBallsTimer.updateSpeed(arenaArgsConfig.speed - 0.1f)
        }
        if (timeNow <= 6) {
            soundManager.playTicSoundEffect()
        }
        if (timeNow == 0) {
            isGameFinished = true
            viewState.backNavigationAction.value = true
            soundManager.playGameFinishSoundEffect()
            soundManager.clearCurrentSoundEffect()
        }
        viewState.timeValue.value = formatDuration(timeNow.toLong())
    }

    private fun addMoreTimeToTimerAndRestartIt(seconds: Int) {
        timeNow = timeNow + seconds
        totalTime = timeNow
        timeTimer.UpdateTakeUntile(timeNow)
        timeTimer.stopTimer()
        startTimeCountDown()
    }

    fun startFlowingBallsWithTimeInterval() {
        flowingBallsTimer.resumeTimer {
            flowingOneRandomBall(it)
        }
    }

    private fun flowingOneRandomBall(it: Long) {
        Log.d(TAG, "startFlowingBallsWithTimeInterval: ${it}")
        val emptyDots = getEmptyDotsOnTheBoard()
        Log.d(TAG, "flowingOneRandomBall: number of empty : {${emptyDots.size}}")
        Log.d(TAG, "flowingOneRandomBall: the empty dot list : {${emptyDots.toString()}}")
        if (emptyDots.isEmpty()) { //board is full
            Log.d(TAG, "flowingOneRandomBall: the board is full")
            isThereScoringDecreasing = true
            scoringDecreasingValue = 10
            flowingBallsTimer.stopTimer()
            soundManager.playBoardCompletedSoundEffect()
        } else if (emptyDots.size == 1 && !isThereAnyMatchinginInTheBoard()) {
            Log.d(TAG, "flowingOneRandomBall: the board is in last dot")
            val emptyDot = emptyDots[0]
            val randomBall = flowingBallsUseCase.provideBestBallForLastEmptyBallInBoard(
                boardMap, arenaArgsConfig.level
            )
            emptyDot.dotView.startDotFlowingWithSpecicalColorAndPowerUp(
                randomBall, currentFlowingPowerUp
            )
            emptyDot.number = randomBall.num
            emptyDot.keyColor = randomBall.key
            Log.d(
                TAG,
                "flowingOneRandomBall: last dot currentFlowingPowerUp: $currentFlowingPowerUp"
            )
            currentFlowingPowerUp?.let {
                emptyDot.number = it.ballNumber
            }
            emptyDot.flowingBirthDate = Date()
            updateDotEmptyState(emptyDot, false)
        } else {
            Log.d(TAG, "flowingOneRandomBall: the board is not full")
            val randomEmptyDot =
                if (emptyDots.size == 1) emptyDots[0] else emptyDots[Skillz.getRandom()
                    .nextInt(0, emptyDots.size - 1)]
            val randomBall = arenaRandom.provideRandomBallDrawable(arenaArgsConfig.level)
            randomEmptyDot.dotView.startDotFlowingWithSpecicalColorAndPowerUp(
                randomBall,
                currentFlowingPowerUp
            )
            randomEmptyDot.keyColor = randomBall.key
            Log.d(TAG, "flowingOneRandomBall: currentFlowingPowerUp: $currentFlowingPowerUp")
            currentFlowingPowerUp?.let {
                randomEmptyDot.number = it.ballNumber
            }
            randomEmptyDot.flowingBirthDate = Date()
            updateDotEmptyState(randomEmptyDot, false)
        }
    }

    private fun isThereAnyMatchinginInTheBoard(): Boolean {
        val numberList = listOf<Int>(1, 2, 4)
        for (i in numberList) {
            val groupNumberDot = boardMap.values.filter {
                it.number == i
            }
            Log.d(TAG, "isThereAnyMatchinginInTheBoard: ${groupNumberDot.size} of the number $i")
            if (groupNumberDot.isNotEmpty() && groupNumberDot.size > 1) {
                groupNumberDot.forEachIndexed { index: Int, dot: Dot ->
                    if (index < groupNumberDot.size - 1) {
                        if (dot.keyColor.equals(groupNumberDot[index + 1].keyColor)) {
                            Log.d(
                                TAG,
                                "isThereAnyMatchinginInTheBoard: there are matching with ${dot.keyColor}"
                            )
                            return true
                        }
                    }
                }
            }
        }
        Log.d(TAG, "isThereAnyMatchinginInTheBoard: there is no matching in the Board")
        return false
    }

    fun getDot(key: String): Dot? {
        return boardMap[key]
    }

    @Synchronized
    fun updateDotEmptyState(dot: Dot, isEmpty: Boolean) {
        Log.d(TAG, "updateDotEmptyState: update key : ${dot.tag} with value ${isEmpty}")
        dot.isEmpty = isEmpty
        if (isEmpty) dot.keyColor = ""
    }

    @Synchronized
    fun updateDotNumber(dot: Dot, number: Int) {

        dot.updateNumber(number)
    }

    @Synchronized
    private fun getEmptyDotsOnTheBoard(): List<Dot> {
        return boardMap.values.filter {
            it.isEmpty
        }
    }


    fun resumeTimerWithFlowingBalls() {
        if (!isAppFrezzed) {
            isThereScoringDecreasing = false
            scoringDecreasingValue = 0
            flowingBallsTimer.resumeTimer { flowingOneRandomBall(it) }
        }
    }

    fun isMergingPossible(movedDotView: DotCustomView, dropedOnDotView: DotCustomView): Boolean {
        val result = if (dropedOnDotView.isMovableDotVisible()) {
            movedDotView.imgMovableDot.drawable.constantState?.equals(
                dropedOnDotView.imgMovableDot.drawable.constantState
            ) == true && movedDotView.tvNumber.text.equals(dropedOnDotView.tvNumber.text)
        } else
            true
        Log.d(TAG, "isMergingPossible: $result")
        return result
    }

    fun mergeDots(movedDotView: DotCustomView, dropedOnDotView: DotCustomView) {
        //set Visibility
        movedDotView.showMovableDot(false)
        dropedOnDotView.showMovableDot(true)
        val num: Int? = getDot(dropedOnDotView.tag)?.number;
        Log.d(TAG, "mergeDots: $num")
        num?.let {
            when (num) {
                1 ->
                    soundManager.playMerge1SoundEffect()
                2 -> soundManager.playMerge2SoundEffect()
                4 -> soundManager.playMerge3SoundEffect()
                else ->
                    soundManager.playMerge2SoundEffect()
            }
        }
        //update the main Board
        updateBoardWithMerging(movedDotView, dropedOnDotView)


    }

    fun handleNotPossibleMerging(movedDotView: DotCustomView, dropedOnDotView: DotCustomView) {
        //movedDotView.cancelDragAndDrop()
        // movedDotView.moveMovableDotBackToOriginalPoisition(binding.lnArenaMain,event.x,xdelta,event.y,ydelta)
        movedDotView.showMovableDot(true)
        if (dropedOnDotView.keyColor.isNotEmpty() && !movedDotView.keyColor.equals(dropedOnDotView.keyColor)) {
            updatePointsUseCase.resestStreak()
            updatePointsUseCase.decreaseScoring(10)
            soundManager.playIncorrectSoundEffect()
        }
    }

    private fun updateBoardWithMerging(
        movedDotView: DotCustomView,
        droppedOnDotView: DotCustomView
    ) {
        droppedOnDotView.imgMovableDot.setImageDrawable(movedDotView.imgMovableDot.drawable)
        val movedDot = getDot(movedDotView.tag)
        val droppedOnDot = getDot(droppedOnDotView.tag)
        droppedOnDot?.let {
            movedDot?.let {
                if (!droppedOnDot.isEmpty)
                    droppedOnDot.updateNumber(droppedOnDot.number * 2)
                else {
                    //the dropped dot is empty
                    droppedOnDot.updateNumber(movedDot.number)

                }
                // droppedOnDot number is the same movedDotNumber
                updateDotEmptyState(droppedOnDot, false)
                updatePointsUseCase.updateScoreAndPowerUpsOnMerging(movedDot, droppedOnDot)
                check8Reached(droppedOnDot)
                updateDotEmptyState(movedDot, true)

                movedDot.updateNumber(1)
            }
        }
//        if (boardMap.values.filter { it.isEmpty }.isEmpty()) {
//            //full board
//            timer.stopTimer()
//        } else {
        resumeTimerWithFlowingBalls()
        //}
    }

    private fun check8Reached(droppedOnDot: Dot) {
        if (droppedOnDot.number == 8) {
            droppedOnDot.dotView.shakeOn8ReachAnimation()
            droppedOnDot.is8Reached = true
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                droppedOnDot.dotView.cancelDragAndDrop()
            } else {
                //todo handle it on api level 23
            }
        }
    }

    fun fullBoard() {
        val balls = boardMap.values.filter {
            !it.isEmpty && !it.is8Reached
        }
        balls.takeLast(balls.size / 2).forEach {
            it.number = 8
            check8Reached(it)
        }
        viewState.clear.value = true
    }

    fun collect8Dots(lnParent: LinearLayout) {
        val filterd8Dots = boardMap.values.filter {
            it.is8Reached
        }
        if (filterd8Dots.isEmpty()) {
            soundManager.playNoEightSoundEffect()
            updatePointsUseCase.decreaseScoring(100)
            updatePointsUseCase.resestStreak()
        } else {
            soundManager.playLevelUpSoundEffect()
            filterd8Dots.forEach {
                it.dotView.cancelShakeAnimation()
                updatePointsUseCase.updateScoreOnCollect8(it)
                it.dotView.startCollectScoreAniamtion(lnParent)
                Handler(Looper.getMainLooper()).postDelayed({
                    it.is8Reached = false
                    it.isEmpty = true
                    it.keyColor = ""
                    it.number = 1
                    it.dotView.tvNumber.text = 1.toString()
                    resumeTimerWithFlowingBalls()
                }, 600L)

                //it.dotView.startCollectScoreAniamtion()
            }

        }
    }

    fun applyPowerUp(pos: Int) {
        val powerUPs = powerUpping.getCurrentPowerUps()

        if (pos < powerUPs.size) {
            val powerUp = powerUpping.getCurrentPowerUps()[pos]
            Log.d(TAG, "applyPowerUp: ${powerUp.name}")
            updatePointsUseCase.applyPowerUpCommon(powerUp)
            if (powerUp.name.equals("color4")) {
                //currentFlowingPowerUp = powerUp
                soundManager.playPowerUpTo4SoundEffect()
                updateCurrentBoardWithThePowerUp(powerUp)
                Handler(Looper.getMainLooper()).postDelayed({
                    //currentFlowingPowerUp = null
                    val initPowerUp = InitialPowerUp()
                    powerUpping.setCurrentPowerUpHeaderText(initPowerUp.name)
                }, powerUp.duration.toLong() * 1000)
            } else if (powerUp.name.equals("frezz")) {
                soundManager.playPowerUpIceSoundEffect()
                addMoreTimeToTimerAndRestartIt(0)
                timeTimer.stopTimer()
                flowingBallsTimer.stopTimer()
                isAppFrezzed = true
                HXMusic.pause()
                soundManager.playFreezePowerUpSoundEffect()
                freezRunnable?.let {
                    freezPowerHandler?.removeCallbacks(it)
                }
                freezRunnable = Runnable {
                    isAppFrezzed = false
                    if (timeNow != 0)
                        resumeTimeTimer()
                    resumeTimerWithFlowingBalls()
                    viewState.resumeMusic.value = true
                    soundManager.clearCurrentSoundEffect()
                    soundManager.playReverseLaserSoundEffect()
                    val initPowerUp = InitialPowerUp()
                    powerUpping.setCurrentPowerUpHeaderText(initPowerUp.name)
                }
                freezPowerHandler = Handler(Looper.getMainLooper())
                freezPowerHandler!!.postDelayed(freezRunnable!!, powerUp.duration.toLong() * 1000)
            } else if (powerUp.name.equals("rush")) {
                soundManager.playRushSoundEffect()
                currentFlowingPowerUp = powerUp
                HXMusic.pause()
                soundManager.playRushPowerUpSoundEffect()
                rushRunnable?.let {
                    rushPowerHandler?.removeCallbacks(it)
                }
                rushRunnable = Runnable {
                    currentFlowingPowerUp = null
                    viewState.resumeMusic.value = true
                    soundManager.clearCurrentSoundEffect()
                    soundManager.playReverseLaserSoundEffect()
                }

                rushPowerHandler = Handler(Looper.getMainLooper())
                rushPowerHandler!!.postDelayed(rushRunnable!!, powerUp.duration.toLong() * 1000)
            } else {
                soundManager.playPowerUp2xSoundEffect()
                viewState.back2xPowerAnimationRunning.value = true
                towXRunnable?.let {
                    towXPowerHandler?.removeCallbacks(it)
                }
                towXRunnable = Runnable {
                    viewState.back2xPowerAnimationRunning.value = false
                    powerUpping.setCurrentPowerUpHeaderText("")
                }
                towXPowerHandler = Handler(Looper.getMainLooper())
                towXPowerHandler?.postDelayed(towXRunnable!!, powerUp.duration.toLong() * 1000)
                updatePointsUseCase.applyPowerUpScoring(powerUp)
                powerUpping.setCurrentPowerUpHeaderText(powerUp.name)
            }
        }
    }

    private fun resumeTimeTimer() {
        timeTimer.resumeTimer {
            resumeTimerLogic(it)
        }
    }

    private fun updateCurrentBoardWithThePowerUp(powerUp: PowerUp) {
        boardMap.values.forEach {
            if (it.keyColor.equals(powerUp.ballKey) && it.number < 8) {
                it.number = 4
                it.dotView.tvNumber.text = 4.toString()
            }
        }
    }

    fun setUp(tvCurrentPowerUp: TextView) {
        powerUpping.setCurrentPowerUpHeaderView(tvCurrentPowerUp)
    }

    fun pauseGame() {
        timeTimer.stopTimer()
        flowingBallsTimer.stopTimer()
        gamePaused = true
    }

    fun resumeGame() {
        totalTime = timeNow
        timeTimer.UpdateTakeUntile(timeNow)
        resumeTimeTimer()
        startFlowingBallsWithTimeInterval()
        gamePaused = false
    }

    fun setupInitialBalls() {
        val numOfInitalBalls: Int = arenaArgsConfig.level * arenaArgsConfig.level / 3
        for (i in 1..numOfInitalBalls - 1) {
            flowingOneRandomBall(i.toLong())
        }
    }

    fun isGamePaused() = gamePaused
    fun isGameEnded() = isGameFinished
    fun handleGamePausing() = if (gamePaused) resumeGame() else pauseGame()

    override fun onCleared() {
        super.onCleared()
        soundManager.clearCurrentSoundEffect()
    }

    fun skillzReportScore(activity: Activity) {
        Skillz.reportScore(activity, BigDecimal(scoring.getTotalScore()))
    }

    fun skillzAbortMatch(activity: Activity) {
        Skillz.abortMatch(activity)
    }

    fun endGmae(activity: Activity) {
        skillzReportScore(activity)
        skillzAbortMatch(activity)
        soundManager.clearCurrentSoundEffect()
        rushRunnable?.let {
            rushPowerHandler?.removeCallbacks(it)
        }
        towXRunnable?.let {
            towXPowerHandler?.removeCallbacks(it)
        }
        freezRunnable?.let {
            freezPowerHandler?.removeCallbacks(it)
        }
        activity.finish()
    }
}