package com.yours.collect8.ui.arena.model

import android.animation.ObjectAnimator
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.yours.collect8.R
import com.yours.collect8.data.RandomBall

abstract class PowerUp(
    var name: String,
    var view: ImageView? = null,
    var powerUpDrawableId: Int? = null,
    var powerUpDrawable: Drawable? = null,
    var duration: Int = 0,//inseconds,
    var ballNumber: Int = 1,
    var ballKey: String = "",
    var description: String = "power up", var multiplayScore: Int = 1
) {
    var oldPos: Float = 0f
    open fun showWithAnimation(show: Boolean) {
        if (show) {
            view?.let {
                oldPos = it.x
                it.x = (-it.width).toFloat()
//                it.animate()?.translationX(0f)?.setDuration(300)?.start()
                val animation =
                    ObjectAnimator.ofFloat(it, "x", 100f).setDuration(2000L)
                animation.start()

            }
        } else {
            view?.let {
                it.animate()?.translationX(oldPos)?.setDuration(300)?.start()
            }

        }
    }
}

class InitialPowerUp() : PowerUp("", null, null, null, 0, 1,"","", 1)

class TowXPowerUp() :
    PowerUp(
        "2x",
        powerUpDrawableId = R.drawable.power_up_2x,
        duration = 10,
        multiplayScore = 2,
        description = "Double points only for 10 seconds on all moves"
    )

class freezPowerUp() :
    PowerUp(
        "frezz",
        powerUpDrawableId = R.drawable.power_up_freez,
        duration = 10,
        multiplayScore = 2,
        description = "Double points only for 10 seconds on all moves"
    )

//todo define Emum With Balls CoLor
class Color4PowerUp(var randomColorBy4Drawable: RandomBall) :
    PowerUp(
        "color4",
        powerUpDrawableId = null, powerUpDrawable = randomColorBy4Drawable.drawable,
        duration = 10,
        ballNumber = 4,ballKey= randomColorBy4Drawable.key,
        description = "Double points only for 10 seconds on all moves"
    )

class RushPowerUp() :
    PowerUp(
        "rush",
        powerUpDrawableId = R.drawable.power_up_rush,
        duration = 10, ballNumber = 4,
        multiplayScore = 1,
        description = "all upcomming balls will be 4 for 10 seconds"
    )
