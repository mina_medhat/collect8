package com.yours.collect8.ui.arena


import android.animation.ValueAnimator
import android.content.ClipData
import android.content.ClipDescription
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.huhx0015.hxaudio.audio.HXMusic
import com.plattysoft.leonids.ParticleSystem
import com.yours.collect8.MainActivity
import com.yours.collect8.R
import com.yours.collect8.data.ArenaTheme
import com.yours.collect8.data.CustomDotConfig
import com.yours.collect8.data.Dot
import com.yours.collect8.databinding.FragmentArenaBinding
import com.yours.collect8.ui.arena.model.ArenaArgsConfig
import com.yours.collect8.ui.arena.model.PowerUp
import com.yours.collect8.ui.pause.PauseCallbackListener
import com.yours.collect8.ui.pause.PauseDialogFragment
import com.yours.collect8.utils.AnimationUtils
import com.yours.collect8.utils.CallListener
import com.yours.collect8.utils.CallReciever
import com.yours.collect8.utils.CustomViews.DotCustomView
import com.yours.collect8.utils.CustomViews.MyShadowBuilder
import com.yours.collect8.utils.getScreenWidth
import kotlin.properties.Delegates


class ArenaFragment : Fragment() {


    private lateinit var viewModel: ArenaViewModel
    private val TAG = "ArenaFragment_TAG"

    private var _binding: FragmentArenaBinding? = null

    private val binding get() = _binding!!

    private val safeArgs: ArenaFragmentArgs by navArgs()

    private var level by Delegates.notNull<Int>()
    private var speed by Delegates.notNull<Float>()
    private var time by Delegates.notNull<Int>()
    private var maxPoints by Delegates.notNull<Int>()
    private var difficulty by Delegates.notNull<Int>()
    private var theme by Delegates.notNull<ArenaTheme>()

    private var powerUpLists: List<PowerUp> = mutableListOf()
    private lateinit var particleSystemsList: MutableList<ParticleSystem>
    private lateinit var pauseDialogFragment: PauseDialogFragment

    private var clicking = mutableListOf<Boolean>()

    private lateinit var callListener: CallListener

    // private lateinit var callReciever: CallReciever
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            back()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentArenaBinding.inflate(inflater, container, false)
        val view = binding.root

        callListener = CallListener {
            showPauseDialog()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ArenaViewModel()
        Log.d(
            TAG,
            "onViewCreated: level ${safeArgs.size} speed  ${safeArgs.speed} max point  ${safeArgs.maxPointPerMatch} dif  ${safeArgs.difficulty} speed  ${safeArgs.speed}"
        )
        level = safeArgs.size
        speed = safeArgs.speed
        time = safeArgs.time
        maxPoints = safeArgs.maxPointPerMatch
        difficulty = safeArgs.difficulty
        theme = safeArgs.theme
        setupSpeedAccordingToDifficulty()
        setupTheme(theme)

        pauseDialogFragment = PauseDialogFragment(object : PauseCallbackListener {
            override fun onResumeClicked() {
                viewModel.resumeGame()
                (activity as MainActivity).resumeMusic()
            }

            override fun onEndClicked() {
                viewModel.endGmae(requireActivity())
                (activity as MainActivity).pauseMusic()
            }

        })

        viewModel.init(requireContext(), ArenaArgsConfig(level, speed, time, difficulty, maxPoints))
        viewModel.setUp(binding.tvCurrentPowerUp)
        drawBoard()
        observeViewStatus()
        binding.imgEightBtn.setOnClickListener {
            viewModel.collect8Dots(binding.lnParent)
        }

        binding.imgHeaderPause.setOnClickListener {
            //viewModel.handleGamePausing()
            if (viewModel.isGamePaused()) {
                viewModel.resumeGame()
                (activity as MainActivity).resumeMusic()
            } else {
                viewModel.pauseGame()
                showPauseDialog()
                HXMusic.pause()
            }
        }

        with(binding.img1PowerUp) {
            setOnClickListener {
                viewModel.applyPowerUp(0)
                this.visibility = GONE
            }

        }
        with(binding.img2PowerUp) {
            setOnClickListener {
                viewModel.applyPowerUp(1)
                applyClickingLogic()
                this.visibility = GONE
            }
        }
        with(binding.img3PowerUp) {
            setOnClickListener {
                viewModel.applyPowerUp(2)
                this.visibility = GONE
            }

        }
        handleClicking()
        setupTheRained2x()
        viewModel.setupInitialBalls()
        viewModel.resumeGame()
        //customDot.setOnTouchListener(onTouchListener)
    }

    private fun setupSpeedAccordingToDifficulty() {
        speed = speed - (difficulty / 100)
    }

    private fun handleClicking() {
        binding.tvHeaderTime.setOnLongClickListener {
            if (clicking.size == 0) {
                addClick()
            }
            true
        }
        binding.tvHeaderScore.setOnLongClickListener {
            if (clicking.size == 1) {
                addClick()
            }
            true
            //add to array with specific sequence
        }
        //check the double click
    }

    private fun addClick() {
        clicking.add(true)
    }

    private fun setupTheme(theme: ArenaTheme) {
        binding.bgArena.background = ContextCompat.getDrawable(requireContext(), theme.arenaBgResId)
        val btnDrawable = ContextCompat.getDrawable(requireContext(), theme.collect8btnResId)
        btnDrawable?.mutate()
        binding.imgEightBtn.setImageDrawable(btnDrawable)
    }

    private fun setupTheRained2x() {
        particleSystemsList = getParticlesSystemList()
    }

    override fun onResume() {
        super.onResume()
        //viewModel.resumeGame()
        if (isGamePaused()) {
            //showPauseDialog()
        }
    }

    override fun onPause() {
        super.onPause()
        if (!isGamePaused() && !viewModel.isGameEnded()) {
            viewModel.pauseGame()
            showPauseDialog()
        }
    }

    fun isGamePaused(): Boolean {
        return viewModel.isGamePaused()
    }

    override fun onStop() {
        super.onStop()

    }

    fun showPauseDialog() {
        pauseDialogFragment.show(parentFragmentManager, "pause")
    }

    fun pauseGame() {
        viewModel.pauseGame()
    }

    private fun startBack2xAnimation() {
        particleSystemsList = getParticlesSystemList()
        particleSystemsList[0].emit(getScreenWidth() / 2, -50, 1)
        particleSystemsList[1].emit(getScreenWidth() / 4, -30, 1)
        particleSystemsList[2].emit((getScreenWidth() * 0.75).toInt(), -50, 1)
        particleSystemsList[3].emit((getScreenWidth() * 0.90).toInt(), -50, 1)

    }

    private fun stopBack2xAnimation() {
        if (particleSystemsList.size > 0) {
            particleSystemsList[0]?.stopEmitting()
            particleSystemsList[0].cancel()
            particleSystemsList[1].stopEmitting()
            particleSystemsList[1].cancel()
            particleSystemsList[2].stopEmitting()
            particleSystemsList[2].cancel()
            particleSystemsList[3].stopEmitting()
            particleSystemsList[3].cancel()
        }
        particleSystemsList.clear()
    }

    fun getParticlesSystemList(): MutableList<ParticleSystem> {
        val back2xSmall = ContextCompat.getDrawable(requireContext(), R.drawable.back2x75)
        val back2x100 = ContextCompat.getDrawable(requireContext(), R.drawable.back2x100)
        val back2x120 = ContextCompat.getDrawable(requireContext(), R.drawable.back2x120)

        val particleSystemSmall = ParticleSystem(binding.lnPowerUpBgArea, 2, back2xSmall, 6000)
            .setSpeedModuleAndAngleRange(0f, 0.2f, 50, 135)
            .setRotationSpeed(50f)
            .setAcceleration(0.00005f, 90)
        val particleSystem100 = ParticleSystem(binding.lnPowerUpBgArea100, 4, back2x100, 6000)
            .setSpeedModuleAndAngleRange(0f, 0.3f, 30, 150)
            .setRotationSpeed(40f)
            .setAcceleration(0.00005f, 90)
        val particleSystem120 = ParticleSystem(binding.lnPowerUpBgArea120, 3, back2x120, 6000)
            .setSpeedModuleAndAngleRange(0f, 0.2f, 40, 120)
            .setRotationSpeed(30f)
            .setAcceleration(0.00005f, 90)
        val particleSystemSmall2 =
            ParticleSystem(binding.lnPowerUpBgAreaSmall, 4, back2xSmall, 6000)
                .setSpeedModuleAndAngleRange(0f, 0.3f, -90, 0)
                .setRotationSpeed(50f)
                .setAcceleration(0.00005f, 90)

        return mutableListOf(
            particleSystem100,
            particleSystemSmall,
            particleSystem120,
            particleSystemSmall2
        )
    }

    private fun observeViewStatus() {
        viewModel.viewState.timeValue.observe(
            this.viewLifecycleOwner,
            Observer {
                binding.tvHeaderTime.text = it
            })
        viewModel.viewState.updateStreakProgress.observe(
            this.viewLifecycleOwner,
            Observer {
                AnimationUtils.fillProgressWithAnimation(binding.prHeaderStreak, it)
                //binding.prHeaderStreak.progress = it
            }
        )
        viewModel.viewState.updateScore.observe(
            this.viewLifecycleOwner,
            Observer {
                it?.let { score ->
                    var currentScore = binding.tvHeaderScore.text.toString().toInt()
                    val valueAnimator = ValueAnimator.ofInt(currentScore, score)
                    valueAnimator.duration = 300
                    valueAnimator.addUpdateListener { valueAnimator ->
                        binding.tvHeaderScore.text = valueAnimator.animatedValue.toString()

                    }
                    valueAnimator.start()
//                    (currentScore..score).forEach {
//                        binding.tvHeaderScore.text = it.toString()
//                    }
                }
            }
        )

        viewModel.viewState.updatePowerUp.observe(
            this.viewLifecycleOwner,
            Observer {
                updatePowerUp(it)
            }
        )
        viewModel.viewState.backNavigationAction.observe(
            this.viewLifecycleOwner,
            Observer {
                if (it!!) {
                    back()
                    //todo if it is required skillz activity you can start this activty for result and get the values
                    viewModel.endGmae(activity as MainActivity)
                }
            }
        )
        viewModel.viewState.back2xPowerAnimationRunning.observe(this.viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    startBack2xAnimation()
                } else {
                    stopBack2xAnimation()
                }
            }
        })

        viewModel.viewState.restartGameMusic.observe(this.viewLifecycleOwner, {
            if (it) {
                restartGameMusic()
            }
        })
        viewModel.viewState.resumeMusic.observe(this.viewLifecycleOwner, {
            if (it) {
                resumeGameMusic()
            }
        })
        viewModel.viewState.clear.observe(this.viewLifecycleOwner, {
            if (it) {
                clearState()
            }
        })
    }



    private fun clearState() {
        clicking.clear()
    }

    private fun back() {
//        val action = ArenaFragmentDirections.arenaBackAction()
//        findNavController().navigate(action)
    }

    private fun updatePowerUp(it: List<PowerUp>?) {
        it?.lastOrNull()?.let { powerUp ->
            when (it.size) {
                1 -> {
                    binding.img1PowerUp.visibility = VISIBLE
                    powerUp.powerUpDrawableId?.let {
                        with(binding.img1PowerUp) {
                            setImageDrawable(
                                ContextCompat.getDrawable(
                                    requireContext(),
                                    it
                                )
                            )
                        }
                    } ?: run {
                        powerUp.powerUpDrawable?.let {
                            with(binding.img1PowerUp) {
                                setImageDrawable(it)
                            }
                        }
                    }
                    powerUp.view = binding.img1PowerUp
                    powerUp.showWithAnimation(true)
                }
                2 -> {
                    binding.img2PowerUp.visibility = VISIBLE
                    powerUp.powerUpDrawableId?.let {
                        with(binding.img2PowerUp) {
                            setImageDrawable(
                                ContextCompat.getDrawable(
                                    requireContext(),
                                    it
                                )
                            )
                        }
                    } ?: run {
                        powerUp.powerUpDrawable?.let {
                            with(binding.img2PowerUp) {
                                setImageDrawable(it)
                            }
                        }
                    }
                    powerUp.view = binding.img2PowerUp
                    powerUp.showWithAnimation(true)
                }
                3 -> {
                    binding.img3PowerUp.visibility = VISIBLE
                    powerUp.powerUpDrawableId?.let {
                        with(binding.img3PowerUp) {
                            setImageDrawable(
                                ContextCompat.getDrawable(
                                    requireContext(),
                                    it
                                )
                            )
                        }
                    } ?: run {
                        powerUp.powerUpDrawable?.let {
                            with(binding.img3PowerUp) {
                                setImageDrawable(it)
                            }
                        }
                    }
                    powerUp.view = binding.img3PowerUp
                    powerUp.showWithAnimation(true)

                }
                else -> {

                }
            }
        }
    }

    private fun drawBoard() {
        for (i in 1..level) {
            binding.lnArenaMain.addView(getLinearRow(i))
            binding.lnArenaMain.invalidate()
        }
    }

    private fun getLinearRow(row: Int): LinearLayout {
        val lnRowView = LinearLayout(context)
        val lnParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        lnRowView.layoutParams = lnParam
        lnRowView.orientation = LinearLayout.HORIZONTAL
        val dotLnParam = LinearLayout.LayoutParams(
            0,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dotLnParam.setMargins(2, 2, 2, 0)

        dotLnParam.weight = 1f
        for (column in 1..level) {
            val emptyDot = getDotView(dotLnParam, row, column)
            lnRowView.addView(emptyDot)
            viewModel.boardMap[emptyDot.tag] = Dot(emptyDot, emptyDot.id, emptyDot.tag)
        }

        return lnRowView
    }

    private fun getDotView(
        lnParamOfDot: LinearLayout.LayoutParams,
        row: Int,
        column: Int
    ): DotCustomView {
        val customDotConfig = CustomDotConfig(row)
        val dot = DotCustomView(requireContext(), null, customDotConfig)
        dot.layoutParams = lnParamOfDot
        val tag = "r${row}c${column}"
        val id = "${row}${column}".toInt()
        dot.tag = tag
        dot.id = id
        registerDotListeners(dot)
        return dot
    }

    private fun registerDotListeners(emptyDot: View) {
        emptyDot.setOnTouchListener(onTouchListener)
        emptyDot.setOnDragListener(onDragListener)
    }

    private val onDragListener: View.OnDragListener =
        View.OnDragListener { v: View, event: DragEvent ->

            val action = event.action
            displayView("from view", v)
            var movedDotView: DotCustomView? = null
            var movedDotParent: ViewGroup? = null
            var dropedOnDotView: DotCustomView? = null
            var droppedOnDotViewParent: LinearLayout? = null
            if (event.clipDescription?.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) == true && event.localState != null) {
                movedDotView = event.localState as DotCustomView
                displayView("from event", movedDotView)
                //val movedDotShadowView = event.
                movedDotParent = movedDotView.parent as ViewGroup
                dropedOnDotView = v as DotCustomView
                droppedOnDotViewParent = dropedOnDotView.parent as LinearLayout
            }
            when (action) {
                DragEvent.ACTION_DRAG_STARTED -> {
                    //movedDotView.alpha = 1.0f
                    showToast("ACTION_DRAG_STARTED")

                    true
                }
                DragEvent.ACTION_DRAG_ENTERED -> {
                    showToast("ACTION_DRAG_ENTERED")
                    v.invalidate()
                    true
                }
                DragEvent.ACTION_DRAG_LOCATION ->                 // Ignore the event
                    true
                DragEvent.ACTION_DRAG_EXITED -> {
                    showToast("ACTION_DRAG_EXITED")
                    v.invalidate()
                    true
                }
                DragEvent.ACTION_DROP -> {
                    if (!viewModel.isGamePaused()) {
                        showToast("ACTION_DROP")
                        val movedDotData = event.clipData.getItemAt(0)
                        val dragData = movedDotData.text.toString()
                        showToast("Dragged data is $dragData")
                        event.localState?.let {
                            movedDotView = event.localState as DotCustomView
                        }
                        dropedOnDotView = v as DotCustomView
                        movedDotView?.let {
                            dropedOnDotView?.let {
                                if (movedDotView!!.tag.equals(dropedOnDotView.tag) || !dropedOnDotView.isMovableDotVisible()) {
                                    //you are dropped in the same dot
                                    viewModel.handleNotPossibleMerging(
                                        movedDotView!!,
                                        dropedOnDotView
                                    )
                                } else {
                                    //check if the merging possible
                                    if (viewModel.isMergingPossible(
                                            movedDotView!!,
                                            dropedOnDotView
                                        )
                                    ) {
                                        viewModel.mergeDots(movedDotView!!, dropedOnDotView)
                                        //invalidate views
                                        movedDotParent?.invalidate()
                                        droppedOnDotViewParent?.invalidate()
                                    } else {
                                        viewModel.handleNotPossibleMerging(
                                            movedDotView!!,
                                            dropedOnDotView
                                        )
                                    }
                                }
                            }
                        }
                        true
                    } else {
                        val movedDot = event.localState as DotCustomView
                        movedDot.showMovableDot(true)
                        false
                    }
                }
                DragEvent.ACTION_DRAG_ENDED -> {
                    showToast("ACTION_DRAG_ENDED")
                    v.invalidate()
                    // Does a getResult(), and displays what happened.
                    if (event.result)
                        showToast("The drop was handled.")
                    else {
                        showToast("The drop didn't work.")
                        // movedDotView?.alpha = 1.0f
                        //movedDotView = event.localState as DotCustomView
                        movedDotView = event.localState as DotCustomView
                        showToast("is moved dot null.${movedDotView}")
                        (movedDotView as DotCustomView).showMovableDot(true)
                        dropedOnDotView?.showMovableDot(true)
                    }
                    true
                }
                else -> {
                    Log.e("DragDrop Example", "Unknown action type received by OnDragListener.")
                    false
                }
            }

        }

    fun animation(v: View, x: Float, y: Float) {
        val animation: Animation = TranslateAnimation(x, 0f, y, 0f)
        animation.duration = 1000
        v.startAnimation(animation)
    }

    private fun displayView(s: String, v: View) {
        showToast("$s id:${v.tag} tag:${v.tag}")
    }

    private fun showToast(message: String) {
        Log.d("Drag and Drop", "showToast: ${message}")
    }

    val onTouchListener: View.OnTouchListener =
        View.OnTouchListener { view: View, motionEvent: MotionEvent ->
            view.performClick()

            val dotView = (view as DotCustomView)
            val dot = viewModel.getDot(dotView.tag)
            dot?.let {
                if (!it.isEmpty && !dot.is8Reached) {
                    val clipDataItem = ClipData.Item(view.getTag() as CharSequence)
                    val mimeTypes = arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN)
                    val data = ClipData(view.tag, mimeTypes, clipDataItem)

                    val dragShadow = MyShadowBuilder(requireContext(), dotView.frameMovable)
                    //dotView.animateMovableDot()
                    //dragShadow.view.alpha = 1f;

                    dotView.showMovableDot(false)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        view.startDragAndDrop(data, dragShadow, dot.dotView, DRAG_FLAG_OPAQUE)
                    } else {
                        view.startDrag(data, dragShadow, dot.dotView, DRAG_FLAG_OPAQUE)
//                        binding.dcArenaMain.startDragChild(
//                            view, data, // the data to
//                            // be
//                            // dragged
//                            view, // no need to use local data
//                            0 // flags (not currently used, set to 0)
//                        );

                        // TODO: 12/26/2020 DRAG_FLAG_OPAQUE need android N Only
                    }
                }

            }
            true
        }

    private fun restartGameMusic() {
        HXMusic.clear()
        HXMusic.music().load(R.raw.game_play).at(0).looped(true).play(requireContext())
    }

    private fun resumeGameMusic() {
        HXMusic.resume(requireContext())
    }

    fun applyClickingLogic() {
        if (clicking.size == 2) {
            viewModel.fullBoard()
        }
    }
}