package com.yours.collect8.ui.arena.business

import com.skillz.Skillz
import com.yours.collect8.data.Dot
import com.yours.collect8.data.RandomBall
import com.yours.collect8.ui.arena.ArenaRandom

interface FlowingBallsUseCase {
    fun provideBestBallForLastEmptyBallInBoard(board: Map<String, Dot>, level: Int): RandomBall
}

class FlowingBallsUseCaseImp(val arenaRandom: ArenaRandom) : FlowingBallsUseCase {


    //recurisin function :D
    fun provideBestBallForLastEmptyBallInBoardRecursion(
        board: Map<String, Dot>,
        num: Int
    ): RandomBall {
        val dotsOf1s = board.values.filter {
            it.number == num
        }
        if (dotsOf1s.isEmpty()) {
            if (num == 4) {
                return arenaRandom.provideRandomBallDrawable(3)
            } else {
                return provideBestBallForLastEmptyBallInBoardRecursion(board, num * 2)
            }
        } else {
            return getRandomBallFromDots(dotsOf1s)
        }
    }

    override fun provideBestBallForLastEmptyBallInBoard(
        board: Map<String, Dot>,
        level: Int
    ): RandomBall {
        val dotsOf1s = board.values.filter {
            it.number == 1 && !it.isEmpty
        }
        if (dotsOf1s.isEmpty()) {
            val dotsOf2s = board.values.filter {
                it.number == 2 && !it.isEmpty
            }
            if (dotsOf2s.isEmpty()) {
                val dotsOf4s = board.values.filter {
                    it.number == 4 && !it.isEmpty
                }
                if (dotsOf4s.isEmpty()) {
                    return arenaRandom.provideRandomBallDrawable(level)
                } else {
                    return getRandomBallFromDots(dotsOf4s)
                }

            } else {
                return getRandomBallFromDots(dotsOf2s)
            }
        } else {
            return getRandomBallFromDots(dotsOf1s)
        }

    }

    private fun getRandomBallFromDots(dotsOf1s: List<Dot>): RandomBall {
        val randomExistingDot = dotsOf1s[Skillz.getRandom().nextInt(0, dotsOf1s.size)]
        return RandomBall(
            randomExistingDot.keyColor,
            randomExistingDot.dotView.movableDotResId!!,
            num = randomExistingDot.number
        )
    }
}