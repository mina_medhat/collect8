package com.yours.collect8.ui.splash

import android.view.View
import android.view.ViewPropertyAnimator
import androidx.lifecycle.ViewModel

class SplashViewModel : ViewModel() {
    init {

    }

    fun animateViewWithScaleOut(view : View) : ViewPropertyAnimator{
        return view.animate().scaleX(0.0f).scaleY(0.0f)
    }

    override fun onCleared() {
        super.onCleared()
    }
}