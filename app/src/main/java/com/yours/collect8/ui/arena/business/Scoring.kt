package com.yours.collect8.ui.arena.business


import com.yours.collect8.data.Dot
import com.yours.collect8.ui.arena.model.PowerUp


interface Scoring {
    fun setCurrentUsagePowerUp(powerUp: PowerUp)
    fun calculateMoveScore(movedDot: Dot, droppedOnDot: Dot): Int
    fun isShouldIncreaseThePowerUpProgress(points: Int): Boolean
    fun calculateAndAdd8ReachedScore(dot: Dot)
    fun addToScore(points: Int)
    fun decreaseScore(points: Int)
    fun resetScore()
    fun getTotalScore(): Int
}

class ScoringImp(private val maxPoint: Int) : Scoring {

    private var score: Int = 0
    private var currentPowerUp: PowerUp? = null

    override fun setCurrentUsagePowerUp(powerUp: PowerUp) {
        currentPowerUp = powerUp
    }

    override fun calculateMoveScore(movedDot: Dot, droppedOnDot: Dot): Int {
        var multiplayScore = 1
        currentPowerUp?.let {
            multiplayScore = it.multiplayScore
        }

        val average =
            (movedDot.getFlowingBirthDateTimeInterVal() + droppedOnDot.getFlowingBirthDateTimeInterVal()) / 2

        var points = maxPoint - (average * 5)
        points = if (points < 1) 50 else points * multiplayScore
        return points
    }

    override fun isShouldIncreaseThePowerUpProgress(points: Int): Boolean {
        return points > maxPoint * 0.5
    }

    override fun calculateAndAdd8ReachedScore(dot: Dot) {
        var multiplayScore = 1
        currentPowerUp?.let {
            multiplayScore = it.multiplayScore
        }

        var points = 1000 * multiplayScore
        addToScore(points)
    }

    override fun addToScore(points: Int) {
        score = score + points
    }

    override fun decreaseScore(points: Int) {
        score = score - points
    }

    override fun resetScore() {
        score = 0
    }

    override fun getTotalScore(): Int {
        return score
    }
}
