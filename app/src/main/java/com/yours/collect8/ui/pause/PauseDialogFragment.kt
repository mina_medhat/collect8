package com.yours.collect8.ui.pause

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.yours.collect8.R
import com.yours.collect8.databinding.LayoutPauseDialogBinding
import fr.tvbarthel.lib.blurdialogfragment.BlurDialogEngine




interface PauseCallbackListener {
    fun onResumeClicked()
    fun onEndClicked()
}

class PauseDialogFragment(private val pauseCallbackListener: PauseCallbackListener) : DialogFragment() {

    lateinit var binding: LayoutPauseDialogBinding
    private var mBlurEngine: BlurDialogEngine? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBlurEngine = BlurDialogEngine(activity);
        mBlurEngine?.let {
            it.setBlurRadius(8);
            it.setDownScaleFactor(5f);
            it.debug(false);
            it.setBlurActionBar(true);
            it.setUseRenderScript(true);
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        binding = LayoutPauseDialogBinding.inflate(inflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        return binding.root
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnDialogResume.setOnClickListener {
            pauseCallbackListener.onResumeClicked()
            dismiss()
        }
        binding.btnDialogEnd.setOnClickListener {
            pauseCallbackListener.onEndClicked()
            dismiss()
        }

    }

    override fun onResume() {
        super.onResume()
        mBlurEngine?.onResume(getRetainInstance());
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mBlurEngine?.onDismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBlurEngine?.onDetach()
    }

    override fun onDestroyView() {
        if (dialog != null) {
            dialog?.setDismissMessage(null);
        }
        super.onDestroyView()
    }
}