package com.yours.collect8.ui.skillz

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.skillz.Skillz
import com.yours.collect8.data.ArizonaTheme
import com.yours.collect8.data.EgyptTheme
import com.yours.collect8.data.NewYourTheme
import com.yours.collect8.databinding.FragmentSkillzBinding

class SkillzFragment : Fragment() {
    private var _binding: FragmentSkillzBinding? = null

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSkillzBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btn3x3.setOnClickListener {
            val flowStepNumberArg = 1
            val action = SkillzFragmentDirections.arenaNavigationAction(
                2f,
                3,
                120,
                200,
                theme = EgyptTheme()
            )
            findNavController().navigate(action)
        }

        binding.btn5x5.setOnClickListener {
            val flowStepNumberArg = 1
            val action = SkillzFragmentDirections.arenaNavigationAction(
                2f,
                5,
                120,
                200,
                theme = ArizonaTheme()
            )
            findNavController().navigate(action)
        }

        binding.btn7x7.setOnClickListener {
            val flowStepNumberArg = 1
            val action = SkillzFragmentDirections.arenaNavigationAction(
                2f,
                7,
                120,
                200,
                theme = NewYourTheme()
            )
            findNavController().navigate(action)
        }

        binding.btn10X10.setOnClickListener {
//            Skillz.launch(activity);
        }
    }
}