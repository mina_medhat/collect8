package com.yours.collect8.ui.arena

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

interface TimeIntervalManager {
    fun resumeTimer(successResult: (interval: Long) -> Unit)
    fun resumeTimerWithComplete(successResult: (interval: Long) -> Unit, onComplete: () -> Unit)
    fun UpdateTakeUntile(moreTime: Int)
    fun stopTimer()
    fun isRunning(): Boolean
    fun isStoped(): Boolean
    fun updateSpeed(speed: Float)
}

class TimeIntervalManagerImp(private var speed: Float) : TimeIntervalManager {
    val TAG = "time"
    private var timeIntervalDisposable: Disposable? = null
    private var takeUntil = 0

    constructor(speed: Float, takeUntil: Int) : this(speed) {
        this.takeUntil = takeUntil
    }


    override fun resumeTimer(successResult: (interval: Long) -> Unit) {
        if (timeIntervalDisposable == null) {
            timeIntervalDisposable = if (takeUntil == 0) {
                getTimer(successResult)
            } else {
                getTimerWithTakeUntile(successResult, {
                    //on compelete
                })
            }
        }
    }

    override fun resumeTimerWithComplete(
        successResult: (interval: Long) -> Unit,
        onComplete: () -> Unit
    ) {
        if (timeIntervalDisposable == null) {
            timeIntervalDisposable = getTimerWithTakeUntile(successResult, onComplete)
        }
    }

    override fun UpdateTakeUntile(newTakeUntile: Int) {
        takeUntil = newTakeUntile
        Log.d(TAG, "UpdateTakeUntileAndRestartTimer: $takeUntil")
    }

    private fun getTimer(successResult: (interval: Long) -> Unit) =
        Observable.interval(0, (speed * 1000).toLong(), TimeUnit.MILLISECONDS).subscribeOn(
            Schedulers.io()
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                successResult(it)
            }, {

            })

    private fun getTimerWithTakeUntile(
        successResult: (interval: Long) -> Unit,
        onComplete: () -> Unit
    ): Disposable? {
        Log.d(TAG, "getTimerWithTakeUntile: ")
        return Observable.interval(0, (speed * 1000).toLong(), TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread()).takeUntil {
                Log.d(TAG, "getTimerWithTakeUntile: $it")
                it == takeUntil.toLong()
            }.subscribe({
                successResult(it)
            }, {

            }, {
                // on complete
                onComplete()
            })
    }

    override fun stopTimer() {
        timeIntervalDisposable?.dispose()
        timeIntervalDisposable = null
    }

    override fun isRunning(): Boolean {
        return timeIntervalDisposable != null && timeIntervalDisposable!!.isDisposed
    }

    override fun isStoped(): Boolean {
        return !isRunning()
    }

    override fun updateSpeed(speed: Float) {
        this.speed = speed
    }
}