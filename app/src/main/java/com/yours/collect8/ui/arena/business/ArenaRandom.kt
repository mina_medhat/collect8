package com.yours.collect8.ui.arena

import android.content.Context
import android.graphics.drawable.LayerDrawable
import androidx.core.content.ContextCompat
import com.skillz.Skillz
import com.yours.collect8.R
import com.yours.collect8.data.RandomBall
import com.yours.collect8.ui.arena.model.*


interface ArenaRandom {
    fun provideRandomBallDrawable(level: Int): RandomBall
    fun provideRandomPowerUpBallDrawable(level: Int): RandomBall
    fun provideRandomColor4PowerUp(level: Int): RandomBall
    fun getRandomPowerUp(currentPowerUp: List<PowerUp>): PowerUp
}

class ArenaRandomImp(val context: Context, val level: Int) : ArenaRandom {


    val ballsArray = listOf<RandomBall>(
        RandomBall("red", R.drawable.reddot1),
        RandomBall("yellow", R.drawable.yellowdot2),
        RandomBall("blue", R.drawable.bluedot3),
        RandomBall("green", R.drawable.greendot),
        RandomBall("orange", R.drawable.orangedot8),
        RandomBall("burgundy", R.drawable.burgundydot5),
        RandomBall("dark_blue", R.drawable.darkbluedot6),
        RandomBall("pink", R.drawable.pinkdot7),
        RandomBall("purple", R.drawable.purpledot9),
        RandomBall("rainbow", R.drawable.rainbowdot10),
    )
    val powerUpColor4Drawable =
        listOf<RandomBall>(
            RandomBall("red", R.drawable.powerupcolor4red),
            RandomBall("yellow", R.drawable.powerupcolor4yellow),
            RandomBall("blue", R.drawable.powerupcolor4blue),
            RandomBall("green", R.drawable.powerupcolor4green),
            RandomBall("orange", R.drawable.powerupcolor4orange),
            RandomBall("burgundy", R.drawable.powerupcolor4burgundy),
            RandomBall("dark_blue", R.drawable.powerupcolor4darkblue),
            RandomBall("pink", R.drawable.powerupcolor4pink),
            RandomBall("Purple", R.drawable.powerupcolor4purple),
            RandomBall("rainbow", R.drawable.powerupcolor4rainbow)
        )


    override fun provideRandomBallDrawable(level: Int): RandomBall {
        val ballsDrawable =
            ballsArray.take(level)
        return ballsDrawable[Skillz.getRandom().nextInt(0, ballsDrawable.size )]
    }

    override fun provideRandomPowerUpBallDrawable(level: Int): RandomBall {
        val balls = powerUpColor4Drawable.take(level)
        return balls[Skillz.getRandom().nextInt(0, balls.size)]
    }

    override fun provideRandomColor4PowerUp(level: Int): RandomBall {
//        val coloredImg = ContextCompat.getDrawable(context, provideRandomBallDrawable())
//        val powerUpColor4Img = ContextCompat.getDrawable(context, R.drawable.by4)
//
//        val finalDrawable = LayerDrawable(arrayOf(coloredImg, powerUpColor4Img))
//        finalDrawable.setLayerInset(0, 0, 0, 0, 0);
//        finalDrawable.setLayerGravity(0, Gravity.CENTER)
//        finalDrawable.setLayerGravity(1, Gravity.CENTER)

        val bgDrawable = ContextCompat.getDrawable(
            context,
            R.drawable.powerup4color
        ) as LayerDrawable /*drawable*/
        val randomBall = provideRandomPowerUpBallDrawable(level)
        bgDrawable.setDrawableByLayerId(
            R.id.img_colored_drawable,
            ContextCompat.getDrawable(context, randomBall.resId)
        );
        randomBall.drawable = bgDrawable

        return randomBall
    }

    override fun getRandomPowerUp(currentPowerUp: List<PowerUp>): PowerUp {
        val powerUpsArray =
            mutableListOf<PowerUp>(
                TowXPowerUp(), Color4PowerUp(
                    provideRandomColor4PowerUp(level)
                ),
                RushPowerUp()
            )

        if (level != 3)
            powerUpsArray.add(freezPowerUp())

        val filteredArray = powerUpsArray.filter {
            !currentPowerUp.contains(it)
        }
        return filteredArray[Skillz.getRandom().nextInt(0, filteredArray.size)]
    }
}