package com.yours.collect8

import android.app.Application
import com.huhx0015.hxaudio.audio.HXMusic

import com.yours.collect8.utils.Prefs


class Collect8App : Application() {
    companion object {
        var prefs: Prefs? = null
    }

    override fun onCreate() {
        super.onCreate()
        //initKoin()

        prefs = Prefs(applicationContext)
    }


    override fun onTerminate() {
        HXMusic.clear()
        super.onTerminate()
    }


//    private fun initKoin() {
//        startKoin {
//            androidLogger(Level.DEBUG)
//            androidContext(this@Collect8App)
//            modules(
//                listOf(
//                    splashModule, arenaModule
//                )
//            )
//        }
//    }
}