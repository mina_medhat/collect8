package com.yours.collect8.data

import android.graphics.drawable.Drawable
import android.util.Log
import com.yours.collect8.utils.CustomViews.DotCustomView
import java.util.*

data class Dot(
    val dotView: DotCustomView,
    val id: Int = 0,
    val tag: String = "",
    var isEmpty: Boolean = true,
    var is8Reached: Boolean = false
) {
    var number: Int = 1
        set(value) {
            field = value
            dotView.tvNumber.text = value.toString()
        }

    var keyColor: String = ""
        set(value) {
            dotView.keyColor = value
            field = value
        }


    var flowingBirthDate: Date? = null
    private var baseDotBirthDate: Date = Date()

    val TAG = "Dot $tag"
    override fun toString(): String {
        return tag
    }

    @Synchronized
    fun updateNumber(num: Int) {
        Log.d(TAG, "updateDotNumber with value ${num}")
        number = num
        dotView.tvNumber.text = num.toString()
    }

    fun getBaseDotBirthDateTimeInterVal(): Int {
        val interval: Long = Date().time - baseDotBirthDate.time
        return (interval / 1000).toInt()
    }

    fun getFlowingBirthDateTimeInterVal(): Int {
        flowingBirthDate?.let {
            Log.d(TAG, "getFlowingBirthDateTimeInterVal: time now: " + Date().time)
            Log.d(TAG, "getFlowingBirthDateTimeInterVal: ball now: " + it.time)
            val interval: Long = Date().time - it.time
            Log.d(TAG, "getFlowingBirthDateTimeInterVal: interval " + interval)
            Log.d(TAG, "getFlowingBirthDateTimeInterVal: interval / 1000 " + (interval / 1000))
            Log.d(
                TAG,
                "getFlowingBirthDateTimeInterVal: interval / 1000 to int " + (interval / 1000).toInt()
            )
            return (interval / 1000).toInt()
        } ?: run {
            return 0
        }
    }
}

data class RandomBall(val key: String, val resId: Int, var num: Int = 1) {
    var drawable: Drawable? = null
}