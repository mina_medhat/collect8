package com.yours.collect8.data

data class CustomDotConfig(val movableDotType: Int = 1, val movableDotVisability: Boolean = false) {
}