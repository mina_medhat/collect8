package com.yours.collect8.data.sound

import android.content.Context
import android.media.MediaPlayer
import com.huhx0015.hxaudio.audio.HXSound
import com.yours.collect8.R

class SoundManager(val context: Context) {

    fun playStartGameSoundEffect() {
        HXSound.sound()
            .load(R.raw.game_start) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playLevelUpSoundEffect() {
        HXSound.sound()
            .load(R.raw.levelup) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playMerge1SoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup1) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playMerge2SoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup2) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playMerge3SoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup3) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }


    fun playRushPowerUpSoundEffect() {
        HXSound.sound()
            .load(R.raw.rush) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playFreezePowerUpSoundEffect() {
        HXSound.sound()
            .load(R.raw.frezr) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playProgressCompeletedSoundEffect() {
        HXSound.sound()
            .load(R.raw.power_up) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playPowerUp2xSoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup_2x) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playPowerUpTo4SoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup_to4) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playRushSoundEffect() {
        HXSound.sound()
            .load(R.raw.laser) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playReverseLaserSoundEffect() {
        HXSound.sound()
            .load(R.raw.reverselaser) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }
    fun playNoEightSoundEffect() {
        HXSound.sound()
            .load(R.raw.no_eight) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playBoardCompletedSoundEffect() {
        HXSound.sound()
            .load(R.raw.boardcomplete) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playPowerUpIceSoundEffect() {
        HXSound.sound()
            .load(R.raw.ice) // Sets the resource of the sound effect. [REQUIRED]
            .play(context);
    }

    fun playIncorrectSoundEffect() {
        HXSound.sound()
            .load(R.raw.powerup4) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playTicSoundEffect() {
        HXSound.sound()
            .load(R.raw.tic) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }

    fun playGameFinishSoundEffect() {
        HXSound.sound()
            .load(R.raw.game_finished) // Sets the resource of the sound effect. [REQUIRED]
            .looped(false)
            .play(context);
    }


    fun pauseCurrentSoundEffict() {
        HXSound.pause();
    }

    fun clearCurrentSoundEffect() {
        HXSound.clear()
    }
}