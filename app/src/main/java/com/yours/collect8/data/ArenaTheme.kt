package com.yours.collect8.data

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.yours.collect8.R
import kotlinx.android.parcel.Parcelize

abstract class ArenaTheme(
    var name: String,
    @DrawableRes var arenaBgResId: Int,
    @DrawableRes var collect8btnResId: Int,
    var primaryColorResId: Int
) : Parcelable
@Parcelize
class EgyptTheme() :
    ArenaTheme("egypt", R.drawable.egypt_background, R.drawable.collect8_btn, R.color.colorAccent)

@Parcelize
class ArizonaTheme() :
    ArenaTheme("arizona", R.drawable.arizona_background, R.drawable.collect8_btn_arizona, R.color.colorAccent)

@Parcelize
class NewYourTheme() :
    ArenaTheme("newyourk", R.drawable.bg_new_york, R.drawable.collect8_btn_new_yourk, R.color.colorAccent)