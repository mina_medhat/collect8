# multidex-keep.pro
-keep class com.google.firebase.crashlytics.** { *; }
-keep class timber.log.** { *; }
-keep class com.facebook.FacebookSdk
-keep class com.path.android.jobqueue.JobManager
-keep class com.yours.collect8.ChangeFingerPrint
-keep class com.yours.collect8.** { *; }
